# VMM Compute Strategies for Selectorless RRAM Arrays

## Description
Official code repository for the manuscript "Analysis of VMM Computation Strategies to implement BNN applications on RRAM arrays".

## Installation
Required python packages: numpy, pandas, matplotlib, seaborn, tensorflow, QKeras, tqdm.

Training code for MLP network is based on https://github.com/BertMoons/QuantizedNeuralNetworks-Keras-Tensorflow and would require files from utils folder of the repository.

## Usage
### MLP Hardware data analysis
#### Training
python train_mlp.py -d thermal_rps -nh 6 -e 40
#### Inference
python analysis_mlp.py
### CNN Simulation-based analysis
#### Training + Inference
python analysis_cnn.py 1 fm
#### Inference
python analysis_cnn.py 0 fm

## Authors and acknowledgment
If you use the dataset as part of your work please cite us using the following:
@misc{Parmar_2022,
author = {V. Parmar, S.K. Kingra, S.Negi, M.Suri},
title = {VMM Compute},
year = {2022},
publisher = {GitLab},
journal = {GitLab repository},
howpublished = {\url{https://github.com/NVM_IITD_Research/VMM_Compute}},
}

## License
MIT License

Copyright (c) 2023 NVM_IITD_Research

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

