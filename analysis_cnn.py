import tensorflow as tf
from qkeras import *
from tensorflow.keras.layers import *
import tensorflow.keras as keras
import os,sys
import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
from tqdm import tqdm
matplotlib.use('Qt5Agg')
import seaborn as sns
num_classes = 10
input_shape = (28, 28, 1)
input_shape2 = (28, 28, 8)
#256/8 = 32
def thermometer_encode(x):
 x2 = np.zeros([x.shape[0],x.shape[1],x.shape[2],8])
 l = 31
 d = 32
 for i in range(8):
  x2[:,:,:,i] = x>l
  l = l + d
 return x2
#use fm for fashion-mnist and m for mnist

if len(sys.argv)>1:
    train = int(sys.argv[1])
    if len(sys.argv)>2:
        ds = sys.argv[2] 
    else:
        ds = 'm'
else:
    train = 0
    ds = 'm'

if ds=='m':
    (x_train0, y_train), (x_test0, y_test) = tf.keras.datasets.mnist.load_data()
else:
    (x_train0, y_train), (x_test0, y_test) = tf.keras.datasets.fashion_mnist.load_data()
x_train = x_train0.astype("float32") / 255
x_test = x_test0.astype("float32") / 255
x_train1 = thermometer_encode(x_train0) 
x_test1 = thermometer_encode(x_test0) 
x_train1 = x_train1.astype("float32")  - 0.5
x_test1 = x_test1.astype("float32") - 0.5

# Make sure images have shape (28, 28, 1)
x_train = np.expand_dims(x_train, -1)
x_test = np.expand_dims(x_test, -1)

y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

bqmodel = keras.Sequential(
    [
        keras.Input(shape=input_shape2),
        QConv2D(20, kernel_size=(5, 5),padding="valid", kernel_quantizer="binary",  bias_quantizer="binary"),
        QActivation("binary"),
        MaxPooling2D(pool_size=(2, 2)),
        QConv2D(50, kernel_size=(5, 5),padding="same", kernel_quantizer="binary",  bias_quantizer="binary"),
        QActivation("binary"),
        MaxPooling2D(pool_size=(2, 2)),
        Flatten(),
        Dropout(0.5),
        QDense(num_classes, kernel_quantizer="binary",  bias_quantizer="binary"),
        Activation("sigmoid"),
    ]
)

batch_size = 128
epochs = 30

bqmodel.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])


mfile = ds+'nist_bin_t.h5'
if train:
 bqmodel.fit(x_train1, y_train, batch_size=batch_size, epochs=epochs, validation_split=0.1, callbacks=[keras.callbacks.ModelCheckpoint(filepath=mfile,save_weights_only=True,monitor='val_accuracy',mode='max',save_best_only=True)])
 print(bqmodel.summary())
else:
 bqmodel.load_weights(mfile)  


o2 = np.argmax(y_test,axis=1)

bw0 = bqmodel.layers[-2].get_weights()[0]
bb0 = bqmodel.layers[-2].get_weights()[1]
bw0a = bqmodel.layers[-2].kernel_quantizer_internal(bw0).numpy()
bb0a = bqmodel.layers[-2].kernel_quantizer_internal(bb0).numpy()



lookup = [0.73,5.52,17.99]
m_g = [0.602,44.88]
s_g = [0.268,1.89]

m_g1 = [30.49,93.04]
s_g1 = [5.56,17.81]


m_gb = [3.0450118749999997, 3.91738475, 4.882009875000001]
s_gb = [ 0.2341735735736088, 0.32013039057536774,0.4697460075867962]

m_ga1 = [9.051599,7.094368375,5.674102375,]
s_ga1 = [0.799079786,0.634650499,0.587462725,]

extractor = keras.Model(inputs=bqmodel.inputs,outputs=[layer.output for layer in bqmodel.layers])
features = extractor(x_test1)
features = [f.numpy() for f in features]

bi0 = features[-3]
bo0 = features[-2]

bo1 = np.argmax(bo0,axis=1)



def estimate_dev_perf(m_g0,m_g1,s_g0,s_g1,bw0a,bb0a,bi0,o2,lab1):
    bw2a = bw0a.copy()
    bb2a = bb0a.copy()
    bw2b = bw0a.copy()
    bb2b = bb0a.copy()
    bw0b = -1 * bw0a
    bb0b = -1 * bb0a
    acc_sim = []
    acc_dev = []

    for i in tqdm(range(10)):

        bw2a[np.where(bw0a>=0)[0],np.where(bw0a>=0)[1]] = np.abs(np.random.normal(m_g1,s_g1,len(np.where(bw0a>=0)[0]))) 
        bw2a[np.where(bw0a<0)[0],np.where(bw0a<0)[1]] = np.abs(np.random.normal(m_g0,s_g0,len(np.where(bw0a<0)[0]))) 
        bb2a[np.where(bb0a>=0)[0]] = np.random.normal(m_g1,s_g1,len(np.where(bb0a>=0)[0])) 
        bb2a[np.where(bb0a<0)[0]] = np.random.normal(m_g0,s_g0,len(np.where(bb0a<0)[0])) 

        bw2b[np.where(bw0a>=0)[0],np.where(bw0a>=0)[1]] = np.abs(np.random.normal(m_g0,s_g0,len(np.where(bw0a>=0)[0]))) 
        bw2b[np.where(bw0a<0)[0],np.where(bw0a<0)[1]] = np.abs(np.random.normal(m_g1,s_g1,len(np.where(bw0a<0)[0]))) 
        bb2b[np.where(bb0a>=0)[0]] = np.random.normal(m_g0,s_g0,len(np.where(bb0a>=0)[0])) 
        bb2b[np.where(bb0a<0)[0]] = np.random.normal(m_g1,s_g1,len(np.where(bb0a<0)[0])) 

        ibo0pw = np.dot(bi0>0,bw0a)+bb0a - np.dot(bi0<0,bw0a) - bb0a
        ibo0nw = -np.dot(bi0>0,bw0b)+bb0b + np.dot(bi0<0,bw0b) - bb0b
        ibo0pi = np.dot(bi0>0,bw0a)+bb0a - np.dot(bi0>0,bw0b)+bb0b
        ibo0ni = np.dot(bi0<0,bw0b) - bb0b-np.dot(bi0<0,bw0a)-bb0a 
        ibo0d = ibo0pi+ibo0ni
        ibo0s = ibo0pi-ibo0ni
        ibo0d2 = np.dot(bi0>0,bw0a)+np.dot(bi0<0,bw0b) 
        
        ibo1pw = np.argmax(ibo0pw,axis=1)
        ibo1nw = np.argmax(ibo0nw,axis=1)
        ibo1pi = np.argmax(ibo0pi,axis=1)
        ibo1ni = np.argmax(ibo0ni,axis=1)
        ibo1d = np.argmax(ibo0d,axis=1)
        ibo1d2 = np.argmax(ibo0d2,axis=1)
        ibo1s = np.argmax(ibo0s,axis=1)

        bo0pw = np.dot(bi0>0,bw2a)+bb2a - np.dot(bi0<0,bw2a) - bb2a
        bo0nw = -np.dot(bi0>0,bw2b)+bb2b + np.dot(bi0<0,bw2b) - bb2b
        bo0pi = np.dot(bi0>0,bw2a)+bb2a -np.dot(bi0>0,bw2b)+bb2b
        bo0ni = np.dot(bi0<0,bw2b) - bb2b-np.dot(bi0<0,bw2a)-bb2a 
        bo0d = bo0pi+bo0ni
        bo0d2 = np.dot(bi0>0,bw2a)+np.dot(bi0<0,bw2b) 
        bo0s = bo0pi-bo0ni

        bo1pw = np.argmax(bo0pw,axis=1)
        bo1nw = np.argmax(bo0nw,axis=1)
        bo1pi = np.argmax(bo0pi,axis=1)
        bo1ni = np.argmax(bo0ni,axis=1)
        bo1d = np.argmax(bo0d,axis=1)
        bo1d2 = np.argmax(bo0d,axis=1)
        bo1s = np.argmax(bo0s,axis=1)

        #acc_modelb = np.sum(o2==bo1)/len(o2)*100

        ct = ['PW','NW','PI','NI','Diff','XNOR']
        acc_dev_pw = np.sum(o2==bo1pw)/len(o2)*100
        acc_dev_nw = np.sum(o2==bo1nw)/len(o2)*100
        acc_dev_pi = np.sum(o2==bo1pi)/len(o2)*100
        acc_dev_ni = np.sum(o2==bo1ni)/len(o2)*100
        acc_dev_d = np.sum(o2==bo1d)/len(o2)*100
        acc_dev_d2 = np.sum(o2==bo1d2)/len(o2)*100
        acc_dev_s = np.sum(o2==bo1s)/len(o2)*100

        acc_sim_pw = np.sum(o2==ibo1pw)/len(o2)*100
        acc_sim_nw = np.sum(o2==ibo1nw)/len(o2)*100
        acc_sim_pi = np.sum(o2==ibo1pi)/len(o2)*100
        acc_sim_ni = np.sum(o2==ibo1ni)/len(o2)*100
        acc_sim_d = np.sum(o2==ibo1d)/len(o2)*100
        acc_sim_d2 = np.sum(o2==ibo1d2)/len(o2)*100
        acc_sim_s = np.sum(o2==ibo1s)/len(o2)*100

        acc_sim += [acc_sim_pw,acc_sim_nw,acc_sim_pi,acc_sim_ni,acc_sim_d,acc_sim_d2]
        acc_dev += [acc_dev_pw,acc_dev_nw,acc_dev_pi,acc_dev_ni,acc_dev_d,acc_dev_d2]

    acc1 = acc_sim+acc_dev
    ct1 = ct*20
    p1 = ['Sim']*60+[lab1]*60
    mw = [0]*60 + [m_g1/m_g0] * 60
    #import pdb; pdb.set_trace()
    df1 = pd.DataFrame({'Platform':p1,'Accuracy':acc1,'Compute Type':ct1, 'MW': mw})
    return df1
#plt.figure(figsize=(20,18))

devt = []
pt = []
acc = []
ct = []
mw = []
for i in range(3):
    for j in range(3):
        if i+j == 0:
            df = estimate_dev_perf(m_gb[j],m_ga1[i],s_gb[j],s_ga1[i],bw0a,bb0a,bi0,o2,'L'+str(i+1)+'H'+str(j+1)) 
            pt = pt + df['Platform'].tolist()
            acc = acc + df['Accuracy'].tolist()
            ct = ct + df['Compute Type'].tolist()          
            mw = mw + df['MW'].tolist()
        else:
            df2 = estimate_dev_perf(m_gb[j],m_ga1[i],s_gb[j],s_ga1[i],bw0a,bb0a,bi0,o2,'L'+str(i+1)+'H'+str(j+1))
            pt = pt + df2['Platform'][60:].tolist()
            acc = acc + df2['Accuracy'][60:].tolist()
            ct = ct + df2['Compute Type'][60:].tolist()               
            mw = mw + df2['MW'][60:].tolist()
            print(len(ct))

df = pd.DataFrame({'Platform':pt,'Accuracy':acc,'Compute Type':ct, 'MW': mw})


df.to_csv('final_acc_results_'+ds+'nist_mw_sim_b.csv')


plt.figure(figsize=(10,8))
sns.lineplot(data=df,x='MW',y='Accuracy',hue='Compute Type')
plt.tight_layout()
plt.show()



devt = []
pt = []
acc = []
ct = []
mw = []
sd = []
for i in range(1,10):

        if i == 0:
            df = estimate_dev_perf(m_gb[0],m_ga1[0],m_gb[0]*(0.05*i),m_ga1[0]*(0.1*i),bw0a,bb0a,bi0,o2,'L'+str(i+1)+'H'+str(i+1)) 
            pt = pt + df['Platform'].tolist()
            acc = acc + df['Accuracy'].tolist()
            ct = ct + df['Compute Type'].tolist()          
            mw = mw + df['MW'].tolist()
            sd = sd + [0.05*i]*len(df)
        else:
            df2 = estimate_dev_perf(m_gb[0],m_ga1[0],m_gb[0]*(0.05*i),m_ga1[0]*(0.1*i),bw0a,bb0a,bi0,o2,'L'+str(i+1)+'H'+str(i+1))
            pt = pt + df2['Platform'][60:].tolist()
            acc = acc + df2['Accuracy'][60:].tolist()
            ct = ct + df2['Compute Type'][60:].tolist()               
            mw = mw + df2['MW'][60:].tolist()
            sd = sd + [0.05*i]*int(len(df2)/2)
            print(len(ct))


df = pd.DataFrame({'Platform':pt,'Accuracy':acc,'Compute Type':ct, 'MW': mw, 'SD': sd})


df.to_csv('final_acc_results_'+ds+'nist_mw_sim1a.csv')
plt.figure(figsize=(10,8))
sns.lineplot(data=df,x='SD',y='Accuracy',hue='Compute Type')
plt.tight_layout()
plt.show()
