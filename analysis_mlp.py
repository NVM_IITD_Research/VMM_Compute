import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_context("paper",font_scale=3)
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['lines.linewidth'] = 3
plt.rcParams['lines.markersize'] = 20
plt.rcParams["font.weight"] = "bold"
plt.rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']


def reduce_output(mat):
	return np.concatenate([mat[:,1].reshape(-1,1)-mat[:,0].reshape(-1,1),mat[:,3].reshape(-1,1)-mat[:,2].reshape(-1,1),mat[:,5].reshape(-1,1)-mat[:,4].reshape(-1,1)],axis=1)

def calc_acc(prob,tgt,cnt):
	outa2 = reduce_output(prob)
	outg2 = np.argmax(outa2,axis=1)
	accg2 = np.sum(np.dot(outg2 == tgt.T,cnt))/float(np.sum(cnt))
	return accg2

def calc_acc_normal(prob,tgt,cnt):
	outg2 = np.argmax(prob,axis=1)
	accg2 = np.sum(np.dot(outg2 == tgt.T,cnt))/float(np.sum(cnt))
	return accg2

gain = 421
Rmap = np.loadtxt('weight_map.csv',delimiter=",") #MOhm 
Gmap = 1e3/Rmap #mS
wmap = (Rmap < 18).astype(np.int) 
wact = np.concatenate([(wmap[1,:]-wmap[0,:]).reshape(1,-1),(wmap[3,:]-wmap[2,:]).reshape(1,-1),(wmap[5,:]-wmap[4,:]).reshape(1,-1)],axis=0)
d2 = np.loadtxt('test_data.csv',skiprows=1,delimiter=",")
inp = d2[:,0:6]
cnt = d2[:,6]
tgt = d2[:,7]
cnt_te = d2[:,8]
posin = (inp>0)*0.8
negin = (inp<0)*0.8
posind = (inp>0)
negind = (inp<0)
posW = wmap[[1,3,5],:]
negW = wmap[[0,2,4],:]

prod_pw = np.dot(posind,posW.T) - np.dot(negind,posW.T) 
prod_nw = -np.dot(posind,negW.T) + np.dot(negind,negW.T)
prod_sum = prod_pw - prod_nw
prod_diff = prod_pw + prod_nw
prod_diff2 = np.dot(posind,posW.T) + np.dot(negind,negW.T)
prod_pi = np.dot(posind,wact.T) 
prod_ni = - np.dot(negind,wact.T)

acc_pw_tr = calc_acc_normal(prod_pw,tgt,cnt)
acc_nw_tr = calc_acc_normal(prod_nw,tgt,cnt)
acc_pi_tr = calc_acc_normal(prod_pi,tgt,cnt)
acc_ni_tr = calc_acc_normal(prod_ni,tgt,cnt)
acc_diff_tr = calc_acc_normal(prod_diff,tgt,cnt)
acc_diff2_tr = calc_acc_normal(prod_diff2,tgt,cnt)

acc_pw_te = calc_acc_normal(prod_pw,tgt,cnt_te)
acc_nw_te = calc_acc_normal(prod_nw,tgt,cnt_te)
acc_pi_te = calc_acc_normal(prod_pi,tgt,cnt_te)
acc_ni_te = calc_acc_normal(prod_ni,tgt,cnt_te)
acc_diff_te = calc_acc_normal(prod_diff,tgt,cnt_te)
acc_diff2_te = calc_acc_normal(prod_diff2,tgt,cnt_te)

#print(acc_pw,acc_nw,acc_pi,acc_ni,acc_sum,acc_diff)
cst1a = ['Diff','XNOR','PW','NW','PI','NI']
ideal_acc_tr = [acc_diff_tr,acc_diff2_tr,acc_pw_tr,acc_nw_tr,acc_pi_tr,acc_ni_tr]
ideal_acc_te = [acc_diff_te,acc_diff2_te,acc_pw_te,acc_nw_te,acc_pi_te,acc_ni_te]
ideal_df = pd.DataFrame({'Compute Strategy':cst1a,'Training':ideal_acc_tr,'Testing':ideal_acc_te})
print("Ideal Accuracies")
print(ideal_df)

outg =  np.argmax(np.dot(inp,wact.T),axis=1)

#import pdb; pdb.set_trace()
accg = np.sum(np.dot(outg == tgt.T,cnt))/float(np.sum(cnt))
outip1 =  np.dot(Gmap,posin.T)
outin1 =  np.dot(Gmap,negin.T)
outip2 =  np.dot(posin,Gmap.T)
outin2 =  np.dot(negin,Gmap.T)
outd2 = outip2 - outin2

outwp = outd2[:,[1,3,5]]
outwn = outd2[:,[0,2,4]]
outd2a = outip2[:,[1,3,5]]+outin2[:,[0,2,4]]
outa2 = reduce_output(outd2)
outg2 = np.argmax(outa2,axis=1)

accd2_tr = np.sum(np.dot(outg2 == tgt.T,cnt))/float(np.sum(cnt))
accp2_tr = calc_acc(outip2,tgt,cnt)
accn2_tr = calc_acc(-outin2,tgt,cnt)
accd2a_tr =  np.sum(np.dot(np.argmax(outd2a,axis=1) == tgt.T,cnt))/float(np.sum(cnt))
accwp2_tr = np.sum(np.dot(np.argmax(outwp,axis=1) == tgt.T,cnt))/float(np.sum(cnt))
accwn2_tr = np.sum(np.dot(np.argmax(-outwn,axis=1) == tgt.T,cnt))/float(np.sum(cnt))

accd2_te = np.sum(np.dot(outg2 == tgt.T,cnt_te))/float(np.sum(cnt_te))
accp2_te = calc_acc(outip2,tgt,cnt_te)
accn2_te = calc_acc(-outin2,tgt,cnt_te)
accd2a_te =  np.sum(np.dot(np.argmax(outd2a,axis=1) == tgt.T,cnt_te))/float(np.sum(cnt_te))
accwp2_te = np.sum(np.dot(np.argmax(outwp,axis=1) == tgt.T,cnt_te))/float(np.sum(cnt_te))
accwn2_te = np.sum(np.dot(np.argmax(-outwn,axis=1) == tgt.T,cnt_te))/float(np.sum(cnt_te))


sim_acc_tr = [accd2_tr,accd2a_tr,accwp2_tr,accwn2_tr,accp2_tr,accn2_tr]
sim_acc_te = [accd2_te,accd2a_te,accwp2_te,accwn2_te,accp2_te,accn2_te]
sim_df = pd.DataFrame({'Compute Strategy':cst1a,'Training':sim_acc_tr,'Testing':sim_acc_te})
print("Simulated Accuracies")
print(sim_df)
rep_acc_tr = [0.75,0.72,0.53,0.75,0.47,0.63]
rep_acc_te = [0.60,0.80,0.80,0.80,0.40,0.80]
rep_df = pd.DataFrame({'Compute Strategy':cst1a,'Training':rep_acc_tr,'Testing':rep_acc_te})
print("Reported Accuracies")
print(rep_df)



cst = ['Diff','XNOR','PW','NW','PI','NI']*3
t1 = ['Ideal']*6+['Simulated']*6+['Experimental']*6
acc_summ = ideal_acc_tr+sim_acc_tr+rep_acc_tr
d1 = pd.DataFrame({'Compute Strategy':cst,'Accuracy':acc_summ,'Method':t1})
d1['Accuracy'] = d1['Accuracy'] *100

cst1 = ['Diff','XNOR','PW','NW','PI','NI']*2
acc_summ1 = rep_acc_tr +  rep_acc_te #[0.8125,0.53125,0.53125,0.75,0.46875,0.5,0.8,0.8,0.8,0.8,0.4,0.1]
t2 = ['Train']*6+['Test']*6
d2 = pd.DataFrame({'Compute Strategy':cst1,'Accuracy':acc_summ1,'Dataset Split':t2})
d2['Accuracy'] = d2['Accuracy'] *100

d1.to_csv('acc_sim_exp1b.csv')
d2.to_csv('acc_train_test1b.csv')

plt.figure(figsize=(18,8))
plt.subplot(1,2,1)
sns.barplot(data=d1,x='Compute Strategy',y='Accuracy',hue='Method',palette="Spectral",edgecolor='k',linewidth=1) #,markers=True)
plt.legend(fontsize=17.5,bbox_to_anchor=(1,1.15),frameon=False,ncol=6)
plt.ylabel('Accuracy (%)')
plt.text(-1.5,-9,'(a)',fontsize=40)
plt.subplot(1,2,2)
sns.barplot(data=d2,x='Compute Strategy',y='Accuracy',hue='Dataset Split',palette="Spectral",edgecolor='k',linewidth=1) #,markers=True)
plt.legend(fontsize=17.5,bbox_to_anchor=(1,1.15),frameon=False,ncol=2)
plt.ylabel('Accuracy (%)')
plt.text(-1.5,-9,'(b)',fontsize=40)
plt.tight_layout()
plt.savefig('fig_acc_exp1b.jpg',dpi=300)
plt.show()


