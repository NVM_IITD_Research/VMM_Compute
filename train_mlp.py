import os
os.environ['PYTHONHASHSEED']=str(14)
import random
random.seed(14)
import numpy as np
np.random.seed(14)
import tensorflow as tf
tf.random.set_seed(14)
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, TensorBoard, LearningRateScheduler
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras.losses import squared_hinge
import argparse
import tensorflow.keras.backend as K
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras import regularizers
from tensorflow.keras.layers import Reshape, Activation, Conv2D, Input, MaxPooling2D, BatchNormalization, Flatten, Dense, Lambda, concatenate
from tensorflow.keras.regularizers import l2
from sklearn.datasets import load_iris,load_wine,load_breast_cancer,load_digits
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from quantized_layers import QuantizedConv2D,QuantizedDense
from quantized_ops import quantized_relu as quantize_op
from binary_layers import BinaryConv2D,BinaryDense
from binary_ops import binary_tanh as binary_tanh_op
import cv2
import tensorflow as tf
config = tf.compat.v1.ConfigProto()
sess = tf.compat.v1.Session(config=config)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

def build_mlp(cf):
    def quantized_relu(x):
        return quantize_op(x,nb=cf.abits)

    def binary_tanh(x):
        return binary_tanh_op(x)

    if cf.dataset == 'iris':
        ishape = 4
        cf.classes = 3
    elif cf.dataset == 'plant':
        ishape = 6
        cf.classes = 2
    elif cf.dataset == 'cancer':
        ishape = 30
        cf.classes = 2
    elif cf.dataset == 'wine':
        ishape = 13
        cf.classes = 3
    elif cf.dataset == 'digits':
        ishape = 64 
        cf.classes = 10
    elif cf.dataset == 'thermal_rps':
        ishape = 64 
        cf.classes = 3        
    else:
        print('Dataset not supported')
        return
    if cf.network_type =='float':
        Conv_ = lambda s, f, i, c: Conv2D(kernel_size=(s, s), filters=f, strides=(1, 1), padding='same', activation='linear',
                                   kernel_regularizer=l2(cf.kernel_regularizer),input_shape = (i,i,c))
        Conv = lambda s, f: Conv2D(kernel_size=(s, s), filters=f, strides=(1, 1), padding='same', activation='linear',
                                   kernel_regularizer=l2(cf.kernel_regularizer))                          
        Act = lambda: LeakyReLU()
    elif cf.network_type=='full-qnn':
        Conv_ = lambda s, f, i,c: QuantizedConv2D(kernel_size=(s, s), H=1, nb=cf.wbits, filters=f, strides=(1, 1),
                                            padding='same', activation='linear',
                                            kernel_regularizer=l2(cf.kernel_regularizer),
                                            kernel_lr_multiplier=cf.kernel_lr_multiplier,input_shape = (i,i,c))
        Conv = lambda s, f: QuantizedConv2D(kernel_size=(s, s), H=1, nb=cf.wbits, filters=f, strides=(1, 1),
                                            padding='same', activation='linear',
                                            kernel_regularizer=l2(cf.kernel_regularizer),
                                            kernel_lr_multiplier=cf.kernel_lr_multiplier)
        Denseq = lambda h: QuantizedDense(units=h,nb=cf.wbits,activation='linear',use_bias=False)                                    
        Act = lambda: Activation(quantized_relu)
    elif cf.network_type=='full-bnn':
        Conv_ = lambda s, f,i,c: BinaryConv2D(kernel_size=(s, s), H=1, filters=f, strides=(1, 1), padding='same',
                                         activation='linear', kernel_regularizer=l2(cf.kernel_regularizer),
                                         kernel_lr_multiplier=cf.kernel_lr_multiplier,input_shape = (i,i,c))
        Conv = lambda s, f: BinaryConv2D(kernel_size=(s, s), H=1, filters=f, strides=(1, 1), padding='same',
                                         activation='linear', kernel_regularizer=l2(cf.kernel_regularizer),
                                         kernel_lr_multiplier=cf.kernel_lr_multiplier)
        Denseq = lambda h: BinaryDense(units=h,activation='linear')                                                                     
        Act = lambda: Activation(binary_tanh)
    else:
        print('Wrong network type, the supported network types in this repo are float, qnn, full-qnn, bnn and full-bnn')


    model = Sequential()
    if cf.network_type=='full-bnn':
        model.add(BinaryDense(cf.h,input_shape=(ishape,),use_bias=False))
        model.add(Act())        
        model.add(BinaryDense(cf.classes,use_bias=False))
    elif cf.network_type=='full-qnn':
        model.add(QuantizedDense(cf.h,nb=cf.wbits,input_shape=(ishape,),use_bias=False))
        model.add(Act())        
        model.add(QuantizedDense(cf.classes,nb=cf.wbits,use_bias=False))
    else:    
        model.add(Dense(cf.h,input_shape=(ishape,),use_bias=False))
        model.add(Act())            
        model.add(Dense(cf.classes,use_bias=False))            
    #model.add(Act())
    model.summary()
    return model

def load_thermal_rps():
	img_path = 'dataset_rps1'
	l1 = sorted(os.listdir(img_path))
	y = np.genfromtxt(img_path+'/labels.txt',dtype=np.uint8)
	print(l1)
	data1 = []
	y2 = []
	#import pdb; pdb.set_trace()
	for i in l1:
		if 'jpg' in i:
			d1 = cv2.imread(img_path+'/'+i,0)
			#d1a = (255*(d1>128)).astype(np.uint8)
			d2 = np.reshape(d1,[-1,])
			data1.append(d2)
			y2.append(int(y[int(i.split('.')[0])]))
	return np.array(data1),np.array(y2)		

def prepare_dataset(X,y,pos,bin1=False):
	if bin1:
		X1 = np.sign((MinMaxScaler().fit_transform(X)*255).astype(np.uint8)*(2./255.)-1)
	else:
		if pos:
			X1 = (MinMaxScaler().fit_transform(X)*255).astype(np.uint8)*(1./255.)
		else:	
			X1 = (MinMaxScaler().fit_transform(X)*255).astype(np.uint8)*(2./255.)-1	
			
	y1=np.eye(max(y)+1)[y]*2-1
	return X1,y1

# parse arguments
parser = argparse.ArgumentParser(description='Model training')
parser.add_argument('-n', '--nw_type', type=str,default='full-bnn', help='Network type')
parser.add_argument('-wb', '--weight_bits', type=int,default=1, help='Bit precision for weights')
parser.add_argument('-ab', '--act_bits', type=int,default=1, help='Bit precision for activation')
parser.add_argument('-d' ,'--dataset', type=str,default='thermal_rps', help='Dataset for training')
parser.add_argument('-wf' ,'--wfile', type=str,default=None, help='Weight file to load')
parser.add_argument('-e', '--epochs', type=int,default=100, help='Epochs of training')
parser.add_argument('-nh', '--hidden', type=int,default=6, help='Hidden layer size')
parser.add_argument('-pos','--pos_inp', default=False, action="store_true" , help="Flag indicate training using only positive input")

class classifier():
	def __init__(self,wb,ab,ds,nt,wf,h,ep):
		self.wbits=wb
		self.abits=ab
		self.dataset=ds
		self.network_type=nt
		self.kernel_regularizer=0.
		self.activity_regularizer=0.
		self.classes = 1
		self.h = h
		# width and depth
		#learning rate decay, factor => LR *= factor
		self.decay_at_epoch = [0, 80, 200 ]
		self.factor_at_epoch = [1, .1, 1]
		self.kernel_lr_multiplier = 10

		# debug and logging
		self.progress_logging = 1 # can be 0 = no std logging, 1 = progress bar logging, 2 = one log line per epoch
		self.epochs = ep
		self.batch_size = 14
		self.lr = 0.01
		self.decay = 0.000025
		if wf==None:		
			self.out_wght_path = './weights_mlp/{}_{}_{}_{}b_{}b.hdf5'.format(self.dataset,self.network_type,self.h,self.abits,self.wbits)
		else:	
			self.out_wght_path = wf

args = parser.parse_args().__dict__
cf = classifier(args['weight_bits'],args['act_bits'],args['dataset'],args['nw_type'],args['wfile'],args['hidden'],args['epochs'])
bin1 = False
if cf.dataset == 'iris':
	X,y = load_iris(return_X_y=True)
elif cf.dataset == 'wine':
	X,y = load_wine(return_X_y=True)	
elif cf.dataset == 'digits':
	X,y = load_digits(return_X_y=True)	
elif cf.dataset == 'cancer':
	X,y = load_breast_cancer(return_X_y=True)
elif cf.dataset == 'plant':
	pc1 = fetch_mldata('plant-classification')
	X = pc1.data[:,:6]
	y = (pc1.data[:,6] - 1)
elif cf.dataset == 'thermal_rps':
	X,y = load_thermal_rps()
	bin1 = True
else:
	print('Dataset not supported')
X,y = prepare_dataset(X,y,args['pos_inp'],bin1)
x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)
model = build_mlp(cf)
early_stop = EarlyStopping(monitor='loss', min_delta=0.001, patience=10, mode='min', verbose=1)

# learning rate schedule
def scheduler(epoch):
    if epoch in cf.decay_at_epoch:
        index = cf.decay_at_epoch.index(epoch)
        factor = cf.factor_at_epoch[index]
        lr = K.get_value(model.optimizer.lr)
        IT = x_train.shape[0]/cf.batch_size
        current_lr = lr * (1./(1.+cf.decay*epoch*IT))
        K.set_value(model.optimizer.lr,current_lr*factor)
        print('\nEpoch {} updates LR: LR = LR * {} = {}\n'.format(epoch+1,factor, K.get_value(model.optimizer.lr)))
    return K.get_value(model.optimizer.lr)
    
lr_decay = LearningRateScheduler(scheduler)

adam= Adam(lr=cf.lr, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=cf.decay)

print('compiling the network\n')
model.compile(loss=squared_hinge, optimizer=adam, metrics=['accuracy'])
print('Training the network\n')
model.fit(x_train,y_train,batch_size = cf.batch_size,epochs = cf.epochs,verbose = cf.progress_logging,callbacks = [lr_decay],validation_data=(x_test,y_test))
model.save_weights(cf.out_wght_path)
score0 = model.evaluate(x_train, y_train, verbose=0)
score1 = model.evaluate(x_test, y_test, verbose=0)
print('Train accuracy:', score0[1])
print('Test accuracy:', score1[1])
print('Done\n')
